import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../../service/users.service';
import { Users } from '../../models/Users.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private usersService: UsersService) { }

  users: Users[];

  ngOnInit(): void {
    this.usersService.getUsers().subscribe(users => {
      this.users = users;
    });
  };

  onCreateUser() {
    this.router.navigate(['newUser'], { relativeTo: this.route });
  }

  onLogin() {
    this.router.navigate(['login'], { relativeTo: this.route });
  }

}
