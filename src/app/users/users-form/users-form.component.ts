import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../../service/users.service';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  constructor(private usersService: UsersService) { }
  userForm: FormGroup;

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit(){
    this.usersService.createUser(this.userForm.value).subscribe(result => {
      console.log(result);
    })
  }

  private initForm(){
    let username = "";
    let password = "";

    this.userForm = new FormGroup({
      "username": new FormControl(username, Validators.required),
      "password": new FormControl(password, Validators.required)
    })
  }

}
