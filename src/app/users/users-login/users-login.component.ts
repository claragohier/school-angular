import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../../service/users.service';

@Component({
  selector: 'app-users-login',
  templateUrl: './users-login.component.html',
  styleUrls: ['./users-login.component.css']
})
export class UsersLoginComponent implements OnInit {

  constructor(private usersService: UsersService) { }

  login: FormGroup;
  ngOnInit(): void {
    this.initForm();
  };

  onSubmit() {
    this.usersService.login(this.login.value).subscribe(login => {
      console.log(login);
    });
  };

  private initForm() {
    let username = "";
    let password = "";

    this.login = new FormGroup({
      "username": new FormControl(username, Validators.required),
      "password": new FormControl(password, Validators.required)
    });
  };


}
