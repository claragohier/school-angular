import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFormEditComponent } from './users-form-edit.component';

describe('UsersFormEditComponent', () => {
  let component: UsersFormEditComponent;
  let fixture: ComponentFixture<UsersFormEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersFormEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
