import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentsComponent } from './students/students.component';
import { StudentsFormComponent } from './students/students-form/students-form.component';
import { StudentsFormEditComponent } from './students/students-form-edit/students-form-edit.component';
import { StudentsDetailsComponent } from './students/students-details/students-details.component';

import { ExercisesComponent } from './exercises/exercises.component';
import { ExercisesFormComponent } from './exercises/exercises-form/exercises-form.component';
import { ExercisesFormEditComponent } from './exercises/exercises-form-edit/exercises-form-edit.component';
import { ExercisesDetailsComponent } from './exercises/exercises-details/exercises-details.component';

import { UsersComponent } from './users/users.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UsersFormEditComponent } from './users/users-form-edit/users-form-edit.component';
import { UsersDetailsComponent } from './users/users-details/users-details.component';
import { UsersLoginComponent } from './users/users-login/users-login.component';

const routes: Routes = [
  // création route de base qui redirige vers /students, pathMatch: url match totalement
  { path: "", redirectTo: "/students", pathMatch: "full" },

  {
    // création route pour StudentsComponent
    path: "students", component: StudentsComponent, children: [
      // route pour création nouveau student (formulaire)
      { path: "newStudent", component: StudentsFormComponent },
      // route pour afficher les détails du student
      { path: ":id", component: StudentsDetailsComponent },
      // route pour mettre à jour le student
      { path: ":id/editStudent", component: StudentsFormEditComponent }
    ]
  },
  // { path: "", redirectTo: "/exercises", pathMatch: "full" },

  {
    // création route pour ExercisesComponent
    path: "exercises", component: ExercisesComponent, children: [
      // route pour afficher tous les exercices
      // { path: "exercises", component: ExercisesListComponent },
      // route pour création nouveau exercice (formulaire)
      { path: "newExercise", component: ExercisesFormComponent },
      // route pour afficher les détails de l'exercice
      { path: ":id/Exercise", component: ExercisesDetailsComponent },
      // route pour mettre à jour l'exercice
      { path: ":id/editExercise", component: ExercisesFormEditComponent }
    ]
  },
  // création route de base qui redirige vers /users, pathMatch: url match totalement
  // { path: "", redirectTo: "/users", pathMatch: "full" },

  {
    // création route pour UsersComponent
    path: "users", component: UsersComponent, children: [
      // route pour création nouvel user
      { path: "newUser", component: UsersFormComponent },
      // route pour login
      { path: "login", component: UsersLoginComponent },
      // route pour afficher les détails de l'user
      { path: ":id/User", component: UsersDetailsComponent },
      // route pour mettre à jour le user
      { path: ":id/editUser", component: UsersFormEditComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],// changer ici pour accéder aux students
  exports: [RouterModule]
})
export class AppRoutingModule { }
