import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Exercises } from '../models/Exercises.model';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class ExercisesService {

  exercises: Exercises;
  url: string = 'http://localhost:2400/exercises/';

  constructor(private http: HttpClient) { }

  getExercises() {
    return this.http.get<Exercises[]>(this.url);
  };

  getExercise(id: string) {
    return this.http.get<Exercises>(this.url + id);
  };

  createExercise(exercise: Exercises) {
    return this.http.post<Response>(this.url, exercise);
  };

  updateExercise(id: string, exercise: Exercises) {
    return this.http.put<Response>(this.url + id, exercise);
  };

  deleteExercise(id: string){
    return this.http.delete<Response>(this.url + id);
  }

}
