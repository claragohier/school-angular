import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from '../models/Users.model';
import { Response } from '../models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  users: Users;
  url: string = 'http://localhost:2400/users/';

  getUsers(){
    return this.http.get<Users[]>(this.url);
  };

  getUser(id: string){
    return this.http.get<Users>(this.url + id);
  };

  createUser(user: Users){
    return this.http.post<Response>(this.url, user);
  };

  updateUser(id: string, user: Users){
    return this.http.put<Response>(this.url + id, user);
  };

  deleteUser(id: string){
    return this.http.delete<Response>(this.url + id);
  };

  login(user: Users){
    return this.http.post(this.url + '/login', user);
  }
}
