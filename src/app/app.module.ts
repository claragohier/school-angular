import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {  FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentsListComponent } from './students/students-list/students-list.component';
import { StudentsFormComponent } from './students/students-form/students-form.component';
import { StudentsFormEditComponent } from './students/students-form-edit/students-form-edit.component';
import { StudentsDetailsComponent } from './students/students-details/students-details.component';
import { StudentsComponent } from './students/students.component';
import { ExercisesListComponent } from './exercises/exercises-list/exercises-list.component';
import { ExercisesFormComponent } from './exercises/exercises-form/exercises-form.component';
import { ExercisesFormEditComponent } from './exercises/exercises-form-edit/exercises-form-edit.component';
import { ExercisesDetailsComponent } from './exercises/exercises-details/exercises-details.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UsersFormEditComponent } from './users/users-form-edit/users-form-edit.component';
import { UsersDetailsComponent } from './users/users-details/users-details.component';
import { UsersComponent } from './users/users.component';
import { UsersLoginComponent } from './users/users-login/users-login.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentsListComponent,
    StudentsFormComponent,
    StudentsFormEditComponent,
    StudentsDetailsComponent,
    StudentsComponent,
    ExercisesListComponent,
    ExercisesFormComponent,
    ExercisesFormEditComponent,
    ExercisesDetailsComponent,
    ExercisesComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersFormEditComponent,
    UsersDetailsComponent,
    UsersComponent,
    UsersLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
