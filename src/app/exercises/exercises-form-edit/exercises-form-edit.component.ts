import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ExercisesService } from '../../service/exercises.service';


@Component({
  selector: 'app-exercises-form-edit',
  templateUrl: './exercises-form-edit.component.html',
  styleUrls: ['./exercises-form-edit.component.css']
})
export class ExercisesFormEditComponent implements OnInit {

  exerciseForm: FormGroup;
  id: string = this.route.snapshot.params.id;

  constructor(private route: ActivatedRoute,
    private exercisesService: ExercisesService) { }

  ngOnInit(): void {
    this.initForm();
  };

  onSubmit(id) {
    this.exercisesService.updateExercise(id, this.exerciseForm.value).subscribe(result => {
      console.log(result);
    });
  };

  private initForm() {
    let exerciseStatement = "";
    let exerciseQuestion = "";
    let exerciseResponse = "";

    this.exerciseForm = new FormGroup({
      "statement": new FormControl(exerciseStatement, Validators.required),
      "question": new FormControl(exerciseQuestion, Validators.required),
      "response": new FormControl(exerciseResponse, Validators.required)
    });
  }

}
