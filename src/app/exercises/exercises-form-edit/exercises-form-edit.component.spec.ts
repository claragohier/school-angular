import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercisesFormEditComponent } from './exercises-form-edit.component';

describe('ExercisesFormEditComponent', () => {
  let component: ExercisesFormEditComponent;
  let fixture: ComponentFixture<ExercisesFormEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExercisesFormEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExercisesFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
