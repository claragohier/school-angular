import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Exercises } from '../../models/Exercises.model';
import { ExercisesService } from '../../service/exercises.service';


@Component({
  selector: 'app-exercises-list',
  templateUrl: './exercises-list.component.html',
  styleUrls: ['./exercises-list.component.css']
})
export class ExercisesListComponent implements OnInit {

  exercises: Exercises[];
  id = this.route.snapshot.params.id;

  constructor(private exercisesService: ExercisesService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.exercisesService.getExercises().subscribe(exercises => {
      this.exercises = exercises;
    });
  };

  onDeleteExercise(id: string) {
    this.exercisesService.deleteExercise(id).subscribe(result => {
      console.log(result);
    });
  };

  onCreateExercise() {
    this.router.navigate(['newExercise'], { relativeTo: this.route });
  }

  onUpdateExercise(id: string) {
    this.router.navigate([id + '/editExercise'], { relativeTo: this.route });
  };

  onGetExercise(id: string){
    this.router.navigate([id + '/Exercise'], { relativeTo: this.route });
  };

}
