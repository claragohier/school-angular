import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExercisesService} from '../../service/exercises.service';
@Component({
  selector: 'app-exercises-details',
  templateUrl: './exercises-details.component.html',
  styleUrls: ['./exercises-details.component.css']
})
export class ExercisesDetailsComponent implements OnInit {

  exercise: any;
  id: string = this.route.snapshot.params.id;

  constructor(private exercisesService: ExercisesService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.exercisesService.getExercise(this.id).subscribe(exercise => {
      this.exercise = exercise;
    });
  };

}
