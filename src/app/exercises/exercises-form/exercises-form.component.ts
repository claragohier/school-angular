import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ExercisesService } from '../../service/exercises.service';

@Component({
  selector: 'app-exercises-form',
  templateUrl: './exercises-form.component.html',
  styleUrls: ['./exercises-form.component.css']
})
export class ExercisesFormComponent implements OnInit {

  exerciseForm: FormGroup;
  constructor(private exercisesService: ExercisesService) { }

  ngOnInit(): void {
    this.initForm();
  };

  onSubmit(){
    this.exercisesService.createExercise(this.exerciseForm.value).subscribe(exercise => {
      console.log(exercise);
    })
  }


  private initForm(){
    let exerciseStatement = "";
    let exerciseQuestion = "";
    let exerciseResponse = "";

    this.exerciseForm = new FormGroup({
      "statement": new FormControl(exerciseStatement, Validators.required),
      "question": new FormControl(exerciseQuestion, Validators.required),
      "response": new FormControl(exerciseResponse, Validators.required)
    });
  };
}
