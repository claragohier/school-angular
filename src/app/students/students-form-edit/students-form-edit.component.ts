import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentsService } from '../../service/students.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students-form-edit',
  templateUrl: './students-form-edit.component.html',
  styleUrls: ['./students-form-edit.component.css']
})
export class StudentsFormEditComponent implements OnInit {

  studentForm: FormGroup;


  constructor(private studentsService: StudentsService,
    private route: ActivatedRoute) { }

  id: string = this.route.snapshot.params.id;

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit(id) {
    if (this.studentForm.valid) {
      this.studentsService.updateStudent(id, this.studentForm.value).subscribe(student => {
        console.log(student);
      });
    }
  }

  private initForm() {
    let studentFirstname = "";
    let studentLastname = "";
    let studentAge = "";
    let studentCity = "";

    this.studentForm = new FormGroup({
      "firstname": new FormControl(studentFirstname, Validators.required),
      "lastname": new FormControl(studentLastname, Validators.required),
      "age": new FormControl(studentAge, Validators.required),
      "city": new FormControl(studentCity, Validators.required)
    })
  }

}
