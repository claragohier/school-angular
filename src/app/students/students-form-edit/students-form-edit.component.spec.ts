import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsFormEditComponent } from './students-form-edit.component';

describe('StudentsFormEditComponent', () => {
  let component: StudentsFormEditComponent;
  let fixture: ComponentFixture<StudentsFormEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentsFormEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
