import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Students } from '../../models/Students.model';
import { StudentsService } from '../../service/students.service';


@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent implements OnInit {

  students: Students[];
  constructor(private studentsService: StudentsService,
    private router: Router,
    private route: ActivatedRoute) { }

  id = this.route.snapshot.params.id;

  ngOnInit(): void {
    this.studentsService.getStudents().subscribe(students => {
      this.students = students;
      // console.log(students);
    });
  };

  onCreateStudent() {
    this.router.navigate(['newStudent'], { relativeTo: this.route });
  };

  onDeleteStudent(id: string) {
    this.studentsService.deleteStudent(id).subscribe(student => {
      console.log(student);
    });
  };

  onUpdateStudent(id: string) {
    this.router.navigate([id + '/editStudent'], { relativeTo: this.route });
  }

  onGetStudent(id: string) {
    this.router.navigate([id], { relativeTo: this.route });
  }
}
