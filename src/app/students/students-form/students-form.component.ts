import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentsService } from '../../service/students.service';

@Component({
  selector: 'app-students-form',
  templateUrl: './students-form.component.html',
  styleUrls: ['./students-form.component.css']
})
export class StudentsFormComponent implements OnInit {

  studentForm: FormGroup;

  constructor(private studentsService: StudentsService) { }

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit() {
    if (this.studentForm.valid) {
      this.studentsService.createStudent(this.studentForm.value).subscribe(student => {
        console.log(student);
      });
    }
  }

  private initForm() {
    let studentFirstname = "";
    let studentLastname = "";
    let studentAge = "";
    let studentCity = "";

    this.studentForm = new FormGroup({
      "firstname": new FormControl(studentFirstname, Validators.required),
      "lastname": new FormControl(studentLastname, Validators.required),
      "age": new FormControl(studentAge, Validators.required),
      "city": new FormControl(studentCity, Validators.required)
    })
  }

}
