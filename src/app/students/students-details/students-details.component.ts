import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../../service/students.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students-details',
  templateUrl: './students-details.component.html',
  styleUrls: ['./students-details.component.css']
})
export class StudentsDetailsComponent implements OnInit {

  constructor(private studentsService: StudentsService,
    private route: ActivatedRoute) { }

    id: string = this.route.snapshot.params.id;
    student: any; 

  ngOnInit(): void {
    this.studentsService.getStudent(this.id).subscribe(result => {
      console.log(result);
      this.student = result;
    });
  };

}
